var Monster = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.maxHP = 100;
		this.curHP = 100;
		this.HPBar = new MobHPBar(this);
		this.atkSpeed = 1;
		this.atkPower = 1;
		this.attackState = 0;
	},
	takeDamage: function(damage) {
		this.curHP -= damage;
	},
	attack: function() {
		this.setPositionX(this.x - 50);
		this.attackState = 1;
	},
	update: function() {
		if(this.attackState == 15) {
			this.setPositionX(this.x + 50);
			this.attackState = 0;
		}
		else if (this.attackState > 0) 
			this.attackState++;
	}
});

var MushroomBoss = Monster.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/mob/mushmom.png');
		this.maxHP = 30;
		this.curHP = 30;
		this.atkSpeed = 3;
		this.atkPower = 3;
	}
});