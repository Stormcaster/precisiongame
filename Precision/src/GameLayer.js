var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 50, 187, 200, 100 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.initStatic();
        var background;
        this.background = new Background();
        this.background.setAnchorPoint(0,0);
        this.addChild(this.background);

        var player;
        this.player = new Player();
        this.player.setPosition(200,250);
        this.player.setAnchorPoint(0,0);
        this.player.scheduleUpdate();
        this.addChild(this.player);

        var playerHP;
        this.playerHP = new PlayerHPBar();
        this.playerHP.setPosition(75,100);
        this.playerHP.setAnchorPoint(0,0);
        this.addChild(this.playerHP);

        var precBar;
        this.precBar = new Bar();
        this.precBar.setPosition(75,150)
        this.precBar.setAnchorPoint(0,0.5);
        this.addChild(this.precBar);

        var impact;
        this.impact = new Impact();
        this.impact.setPosition(625,150);
        this.addChild(this.impact);

        var marker;
        this.marker = new Marker();
        this.marker.setPosition(this.precBar.getPosition());
        this.marker.setTag(GameLayer.markerTag);

        this.state = GameLayer.STATES.IDLE;
        this.addKeyboardHandlers();
        this.scheduleUpdate();

        this.waveCount = 0;
        this.scoreLabel = cc.LabelTTF.create( 'Wave ' + this.waveCount, 'Arial', 40 );
        this.addChild(this.scoreLabel);
        this.scoreLabel.setPosition(700,50);

        return true;
    },
    initStatic: function() {
        GameLayer.arrowsTag = 1;
        GameLayer.markerTag = 2;
        GameLayer.arrowCursor = 100;
        GameLayer.arrowCount = 0;
        GameLayer.arrowsArr = [];
        GameLayer.mobArr = [];
        GameLayer.mobHPBarCursor = 0;
        GameLayer.mobCursor = 0;
        GameLayer.secCount = 0;
        GameLayer.skillEffArr = [];
    },
    mobAttack: function() {
        GameLayer.secCount++;
        if (GameLayer.secCount == 60) {
            for (var i = 0; i < GameLayer.mobArr.length; i++) {
                var random = Math.floor(Math.random() * 10);
                if (GameLayer.mobArr[i].atkSpeed < random) {
                    GameLayer.mobArr[i].attack();
                    this.addChild(new CharHitEffect());
                    this.player.takeDamage(GameLayer.mobArr[i].atkPower);
                }
            }
            GameLayer.secCount = 0;
        }
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function(e) {
                self.onKeyDown(e);
            },
            onKeyReleased: function(e) {
                self.onKeyUp(e);
            }
        }, this);
    },

    checkSkill: function() {
        var temp = [];
        var arrowTemp = GameLayer.arrowsArr.slice(0);
        for (var i = 0; i < GameLayer.arrowsArr.length; i++) {
            var thisArrow = GameLayer.arrowsArr[i];
            temp.push(thisArrow);
            if (temp.toString().indexOf("up,up,up,up,up,down,up,down") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new MegaHeal());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 8, 8);
                temp = [];
            }
            else if (temp.toString().indexOf("up,down,up,down") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new Heal());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 4, 4);
                temp = [];
            }
            else if (temp.toString().indexOf("left,up,right,down,down") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new Genesis());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 5, 5);
                temp = [];
            }
            else if (temp.toString().indexOf("up,down,down,left,down,right") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new Meteor());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 6, 6);
                temp = [];
            }
            else if (temp.toString().indexOf("up,up,down,left,down,right") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new Blizzard());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 6, 6);
                temp = [];
            }
            else if (temp.toString().indexOf("right,right,up,left,left") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new MagicFlare());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 5, 5);
                temp = [];
            }
            else if (temp.toString().indexOf("left,up,right,right") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new MoonShadow());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 4, 4);
                temp = [];
            }
            else if (temp.toString().indexOf("down,up,down") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new Thunderbolt());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 3, 3);
                temp = [];
            }
            else if (temp.toString().indexOf("down,left,up,right") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new IceStrike());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 4, 4);
                temp = [];
            }
            else if (temp.toString().indexOf("right,down,left") > -1) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new IceShot());
                }
                else GameLayer.skillEffArr.push(new Miss());
                Player.initSwingAction();
                arrowTemp.splice(temp.length - 3, 3);
                temp = [];
            }

        }
        for (var i = 0; i < arrowTemp.length; i++) {
            if (GameLayer.mobArr.length > 0) {
                if (this.checkImpact()) {
                    GameLayer.skillEffArr.push(new BasicAttack());
                    Player.initSwingAction();
                }
                else {
                    GameLayer.skillEffArr.push(new Miss());
                    Player.initSwingAction();
                }
            }
        }
    },
    damageOne: function(damage) {
        var thisMob = GameLayer.mobArr[0];
        thisMob.takeDamage(damage);
        this.checkMobDie();
    },
    damageAllMob: function(damage) {
        for (var i = 0; i < GameLayer.mobArr.length; i++) {
            GameLayer.mobArr[i].takeDamage(damage);
        }
    },

    onKeyDown: function(e) {
        if (e == cc.KEY.up) {

            if (GameLayer.arrowCount < 10 && this.state == GameLayer.STATES.PRECBAR) {
                var newUp = new UpArrow();
                newUp.setPosition(new cc.Point(GameLayer.arrowCursor,200));
                newUp.setTag(GameLayer.arrowsTag);
                this.addChild(newUp);

                GameLayer.arrowsArr[GameLayer.arrowsArr.length] = 'up';
                this.advanceArrowCursor();
            }
        }
        else if (e == cc.KEY.down) {

            if (GameLayer.arrowCount < 10 && this.state == GameLayer.STATES.PRECBAR) {
                var newDown = new DownArrow();
                newDown.setPosition(new cc.Point(GameLayer.arrowCursor,200));
                newDown.setTag(GameLayer.arrowsTag);
                this.addChild(newDown);

                GameLayer.arrowsArr[GameLayer.arrowsArr.length] = 'down';                
                this.advanceArrowCursor();
            }
        }
        else if (e == cc.KEY.left) {

            if (GameLayer.arrowCount < 10 && this.state == GameLayer.STATES.PRECBAR) {
                var newLeft = new LeftArrow();
                newLeft.setPosition(new cc.Point(GameLayer.arrowCursor,200));
                newLeft.setTag(GameLayer.arrowsTag);
                this.addChild(newLeft);

                GameLayer.arrowsArr[GameLayer.arrowsArr.length] = 'left';
                this.advanceArrowCursor();
            }   
        }
        else if (e == cc.KEY.right) {

            if (GameLayer.arrowCount < 10 && this.state == GameLayer.STATES.PRECBAR) {
                var newRight = new RightArrow();
                newRight.setPosition(new cc.Point(GameLayer.arrowCursor,200));
                newRight.setTag(GameLayer.arrowsTag);
                this.addChild(newRight);

                GameLayer.arrowsArr[GameLayer.arrowsArr.length] = 'right';
                this.advanceArrowCursor();
            }

        }
        else if (e == cc.KEY.space) {

            if (this.state == GameLayer.STATES.PRECBARSTOPPED) {
                for (var i = 0; i < GameLayer.arrowCount; i++) 
                this.removeChildByTag(GameLayer.arrowsTag, true);

                GameLayer.arrowCursor = 100;
                GameLayer.arrowCount = 0;
                GameLayer.arrowsArr = [];

                this.removeChildByTag(GameLayer.markerTag);
                this.marker.setPosition(this.precBar.getPosition());
                this.marker.resume();
                this.state = GameLayer.STATES.IDLE;
            }
            else if (this.state == GameLayer.STATES.IDLE) {

                this.addChild(this.marker);
                this.marker.scheduleUpdate();

                this.state = GameLayer.STATES.PRECBAR;
            }
            else if (this.state == GameLayer.STATES.PRECBAR) {
                this.marker.pause();
                this.checkSkill();

                this.state = GameLayer.STATES.PRECBARSTOPPED;
            }
        }
    },

    addMonster: function() {
        if (GameLayer.mobArr.length == 0) {
            this.waveCount++;
            var random = Math.floor(Math.random() * 3 + 1);
            for (var i = 0; i < random; i++) {
                var mob = new MushroomBoss();
                mob.maxHP += this.waveCount*5;
                mob.curHP = mob.maxHP;
                mob.attackPower += this.waveCount;
                mob.setAnchorPoint(0,0);
                mob.setPosition(400 + GameLayer.mobCursor,250);
                this.addChild(mob);
                mob.scheduleUpdate();

                var mobHP = mob.HPBar;
                mobHP.setPosition(75,550 - GameLayer.mobHPBarCursor);
                mobHP.setAnchorPoint(0,0);
                mobHP.scheduleUpdate();
                this.addChild(mobHP);

                GameLayer.mobCursor += 100;
                GameLayer.mobHPBarCursor += 25;

                GameLayer.mobArr[GameLayer.mobArr.length] = mob;
            }
            GameLayer.mobCursor = 0;
            GameLayer.mobHPBarCursor = 0;
        }
    },

    advanceArrowCursor: function() {
        GameLayer.arrowCursor += 50;
        GameLayer.arrowCount++;        
    },

    onKeyUp: function(e) {
    },

    updateHPBar: function() {
        this.playerHP.setScaleX(this.player.curHealth/this.player.maxHealth);        
    },

    update: function(dt) {
        if (this.state == GameLayer.STATES.PRECBAR) this.checkMarkerEnd();
        this.addMonster();
        this.updateHPBar();
        this.queueSkillEffect();
        this.checkMobDie();
        this.mobAttack();
        this.scoreLabel.setString('Wave ' + this.waveCount);
        if (this.player.curHealth <= 0)
        cc.director.runScene(new GameOverScene());
    },
    queueSkillEffect: function() {
        if (GameLayer.skillEffArr.length > 0 && Player.animQueue.length > 0) {
            var thisSkill = GameLayer.skillEffArr[0];
            var thisAction = Player.animQueue[0];
            if (!thisSkill.running && this.player.state == Player.STATE.STAND) {
                this.player.stopAllActions();
                this.addChild(thisSkill);
                thisSkill.setPos(this.player.x, this.player.y, GameLayer.mobArr[0].x, GameLayer.mobArr[0].y);
                thisSkill.runEffect();

                this.player.runAction(thisAction);
                this.player.state = Player.STATE.ACT;
            }
            if (thisSkill.isDone() && thisAction.isDone()) {
                this.player.heal(thisSkill.heal);
                if (thisSkill.aoe) {
                    this.damageAllMob(thisSkill.damage);
                } 
                else this.damageOne(thisSkill.damage);

                GameLayer.skillEffArr[0].removeFromParent(true);
                GameLayer.skillEffArr.splice(0,1);

                Player.animQueue.splice(0,1);
                this.player.state = Player.STATE.IDLE;
            }
        }
    },

    checkMobDie: function() {
        var mobList = GameLayer.mobArr;
        for (var i = 0; i < mobList.length; i++) {
            if (mobList[i].curHP <= 0) {
                this.removeChild(mobList[i]);
                this.removeChild(mobList[i].HPBar);
                mobList.splice(i, 1);
            }
        }
    },

    checkMarkerEnd: function() {
        var pos = this.marker.getPosition();
        if (pos.x >= 725) {
            this.marker.pause();
            this.state = GameLayer.STATES.PRECBARSTOPPED;
        }
    },

    checkImpact: function() {
        var posMark = this.marker.getPosition();
        var posImpact = this.impact.getPosition();
        var precision = 100 - Math.abs(posMark.x - posImpact.x)
        var random = Math.floor(Math.random() * 100) + 1;
        if (precision < random) {
            return false;
        }
        else return true;
    },
});

var StartScene = cc.Scene.extend({

    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild(layer);
    }
    
});
GameLayer.STATES = {
    IDLE: 1,
    PRECBAR: 2,
    PRECBARSTOPPED: 3,

};
GameLayer.arrowsTag = 1;
GameLayer.markerTag = 2;
GameLayer.arrowCursor = 100;
GameLayer.arrowCount = 0;
GameLayer.arrowsArr = [];
GameLayer.mobArr = [];
GameLayer.mobHPBarCursor = 0;
GameLayer.mobCursor = 0;
GameLayer.secCount = 0;
GameLayer.skillEffArr = [];