var Marker = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/marker.png');
	},
	update: function(dt) {
		var pos = this.getPosition();
		this.setPosition(pos.x + Marker.SPEED,pos.y);
	},
	setSpeed: function(speed) {
		Marker.SPEED = speed;
	}
});
Marker.SPEED = 6;
