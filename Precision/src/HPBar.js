var HPBar = cc.Sprite.extend({
	ctor: function() {
		this._super();
	},
	update: function() {

	}
});

var PlayerHPBar = HPBar.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/playerhpbar.png');
	},
});

var MobHPBar = HPBar.extend({
	ctor: function(mob) {
		this._super();
		this.initWithFile('res/images/mobhpbar.png');
		this.baseMob = mob;
	},
	update: function() {
		this.setScaleX(this.baseMob.curHP/this.baseMob.maxHP);
	}
});

