var Player = cc.Sprite.extend({
	ctor: function() {
		this._super();
		//this.initWithFile('res/images/player.png');
		this.init();
		//this.setFlippedX(true);
		this.maxHealth = 100;
		this.curHealth = this.maxHealth;
		this.atkPower = 2;
		this.state = Player.STATE.IDLE;
		this.curAction;
		this.swingAnim;
	},
	isStanding: function() {
		return this.state == Player.STATE.STAND;
	},
	attack: function() {
		Player.animQueue.push(this.initSwingAction());
	},

	initIdleAction: function() {
		var idleAnim = cc.Animation.create();
		for (var i = 0; i < 5; i++) {
			var str = 'res/images/charidle/alert_' + i + '.png';
			idleAnim.addSpriteFrameWithFile(str);
		}
		idleAnim.setDelayPerUnit(0.5);
		idleAction = new cc.RepeatForever(new cc.Animate(idleAnim));
		return idleAction;
	},
	
	takeDamage: function(damage) {
		this.curHealth -= damage;
	},
	heal: function(amount) {
		this.curHealth += amount;
		if (this.curHealth > this.maxHealth) this.curHealth = this.maxHealth;
	},
	update: function() {
		if (this.state == Player.STATE.IDLE) {
			this.runAction(this.initIdleAction());
			this.state = Player.STATE.STAND;
		}
	}

});
Player.STATE = {
	IDLE: 1,
	ACT: 2,
	STAND: 3,
};
Player.animQueue = [];
Player.initSwingAction = function() {
	this.swingAnim = cc.Animation.create();
	for(var i = 1; i < 4; i++) {
		var str = 'res/images/charswing/swingO1_' + i + '.png';
		this.swingAnim.addSpriteFrameWithFile(str);
	}
	this.swingAnim.setDelayPerUnit(0.1);
	Player.animQueue.push(new cc.Animate(this.swingAnim));
}

