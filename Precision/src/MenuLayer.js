var MenuLayer = cc.Layer.extend({
	init: function() {
		this._super();
		this.setPosition(0,0);

		this.background = new Menu();
		this.background.setAnchorPoint(0,0);
		this.background.setPosition(0,0);
		this.addChild(this.background);

		this.addKeyboardHandlers();
	},
	addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function(e) {
                self.onKeyDown(e);
            },
            onKeyReleased: function(e) {
                self.onKeyUp(e);
            }
        }, this);
    },
    onKeyUp: function(e) {
    	if (e == cc.KEY.right) {
    		cc.director.runScene(new StartScene());
    	}
    	else if (e == cc.KEY.left) {
    		cc.director.runScene(new TutorialScene());
    	}
    },
    onKeyDown: function(e) {

    }

});

var MenuScene = cc.Scene.extend({
	onEnter: function() {
        this._super();
        var layer = new MenuLayer();
        layer.init();
        this.addChild(layer);
    }

});