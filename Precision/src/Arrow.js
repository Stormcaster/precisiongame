var UpArrow = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/up.png');
	}
});

var DownArrow = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/down.png');
	}
});

var LeftArrow = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/left.png');
	}
});

var RightArrow = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/right.png')
	}
});