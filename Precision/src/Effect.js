var Effect = cc.Sprite.extend({
	ctor: function() {
		this._super();
	}
});

var CharHitEffect = Effect.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/charhit.png');
		this.setPosition(235, 285);
		this.scheduleUpdate();
	},
	update: function() {
		this.setOpacity(this.getOpacity() - 15);
		if (this.getOpacity() == 0) {
			this.removeFromParent();
		}
	}
});

var SwingAfterEffect = Effect.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/swingafter.png')
		this.setPosition(235, 285);
		this.scheduleUpdate();
	},
	update: function() {
		this.setOpacity(this.getOpacity() - 15);
		if (this.getOpacity() == 0) {
			this.removeFromParent();
		}
	}
})