var Skill = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.init();
		this.animateAction;
		this.animFrames = [];
		this.damage = 0;
		this.heal = 0;
		this.aoe = false;
		this.animation = cc.Animation.create();
		this.animation.setDelayPerUnit(0.07);
		this.playerAnim;
		this.running = false;
	},
	runEffect: function() {
		this.running = true;
		this.runAction(this.animateAction);
	},
	isDone: function() {
		return this.animateAction.isDone();
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(mobX, mobY);
	}
});
var BasicAttack = Skill.extend({
	ctor: function() {
		this._super();
		this.damage = 2;
		this.playerAnim = Player.initSwingAction();
		for(var i = 0; i < 5; i++) {
			var str = 'res/images/skills/basic/CharLevel.20.hit.0_' + i + '.png';
			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(mobX + 25, 200);
		this.setAnchorPoint(0.25,0);
	}
});

var Heal = Skill.extend({
	ctor: function() {
		this._super();
		this.heal = 30;
//		cc.spriteFrameCache.addSpriteFrames('res/images/skills/Heal.plist');
		for (var i = 0; i < 15; i++) {
			var str = "res/images/skills/heal/" + i + ".png";
//			var frame = cc.spriteFrameCache.getSpriteFrame(str);
//			animFrames.push(frame);
			this.animation.addSpriteFrameWithFile(str);
		}
//		var animation = new cc.Animation(animFrames, 0.1);
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(playerX + 55, playerY - 10);
		this.setAnchorPoint(0.5,0);
	}
});

var MegaHeal = Skill.extend({
	ctor: function() {
		this._super();
		this.heal = 70;
		for (var i = 0; i < 17; i++) {
			var str = "res/images/skills/megaheal/" + i + ".png";
	//		var frame = cc.spriteFrameCache.getSpriteFrame(str);
	//		animFrames.push(frame);
			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(playerX + 55, playerY - 20);
		this.setAnchorPoint(0.5,0);
	}
});
var Blizzard = Skill.extend({
	ctor: function() {
		this._super();
		this.aoe = true;
		this.damage = 25;
		for (var i = 0; i < 24; i++) {
			var str = 'res/images/skills/blizzard/' + i + '.png';

			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(400,200);
		this.setAnchorPoint(0,0);
	}
});

var MagicFlare = Skill.extend({
	ctor: function() {
		this._super();
		this.damage = 30;
		for(var i = 0; i < 34; i++) {
			var str = 'res/images/skills/magicflare/' + i + '.png';
			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(mobX - 75, 125);
		this.setAnchorPoint(0,0);
	}
});

var MoonShadow = Skill.extend({
	ctor: function() {
		this._super();
		this.damage = 12;
		this.aoe = true;
		for(var i = 0; i < 14; i++) {
			var str = 'res/images/skills/moonshadow/effect_' + i + '.png';
			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(300,125);
		this.setAnchorPoint(0,0);
	}
});

var Meteor = Skill.extend({
	ctor: function() {
		this._super();
		this.aoe = true;
		this.damage = 25;
		for (var i = 0; i < 24; i++) {
			var str = 'res/images/skills/meteor/tile.2_' + i + '.png';

			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(400,240);
		this.setAnchorPoint(0,0);
	}
});
var Genesis = Skill.extend({
	ctor: function() {
		this._super();
		this.damage = 25;
		for (var i = 0; i < 22; i++) {
			var str = 'res/images/skills/genesis/tile.2_' + i + '.png';

			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(mobX + 50, 250);
		this.setAnchorPoint(0.5, 0);
	}
});

var Thunderbolt = Skill.extend({
	ctor: function() {
		this._super();
		this.damage = 15;
		for (var i = 0; i < 10; i++) {
			var str = 'res/images/skills/thunderbolt/hit.0_' + i + '.png';

			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(mobX + 75, 200);
		this.setAnchorPoint(0.5, 0);
	}
});

var IceStrike = Skill.extend({
	ctor: function() {
		this._super();
		this.damage = 10;
		this.aoe = true;
		for(var i = 0; i < 13; i++) {
			var str = 'res/images/skills/icestrike/effect_' + i + '.png';
			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(375,233);
		this.setAnchorPoint(0,0);
	}
});

var IceShot = Skill.extend({
	ctor: function() {
		this._super();
		this.damage = 20;
		for (var i = 0; i < 13; i++) {
			var str = 'res/images/skills/iceshot/hit.0_' + i + '.png';

			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(mobX - 25, 250);
		this.setAnchorPoint(0, 0);
	}
});

var Miss = Skill.extend({
	ctor: function() {
		this._super();
		this.animation.setDelayPerUnit(0.02);
		for (var i = 0; i < 17; i++) {
			var str = 'res/images/skills/miss/' + i + '.png';
			this.animation.addSpriteFrameWithFile(str);
		}
		this.animateAction = new cc.Animate(this.animation);
	},
	setPos: function(playerX, playerY, mobX, mobY) {
		this.setPosition(playerX + 50, playerY + 100);
	}
})
