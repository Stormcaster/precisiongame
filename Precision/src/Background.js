var Background = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/background.png');
	}
});

var Menu = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/menu.png');
	}
});
var Ttrl = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/Tutorial.png');
	}
});
var GameOverBG = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/GameOver.png');
	}
});
